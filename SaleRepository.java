package com.shop.demo.repo;

import org.springframework.data.repository.CrudRepository;

import com.shop.demo.model.Sale;

public interface SaleRepository extends CrudRepository<Sale, Integer>{

}
