package com.shop.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import static org.springframework.boot.SpringApplication.run;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })

public class ShopApplication {

	public static void main(String[] args) {
		run(ShopApplication.class, args);
		System.out.println("Hello world!!");
	}
}
