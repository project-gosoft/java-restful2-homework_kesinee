package com.shop.demo.repo;

import org.springframework.data.repository.CrudRepository;

import com.shop.demo.model.Shop;

public interface ShopRepository extends CrudRepository<Shop, Integer> {

}
